# Read me

This repository stores a demo of the topic types used for documentation at GitLab.

To view the generated output, go here: http://cars.178.62.207.141.nip.io/cars/cars/.

To generate the output, run the pipeline here: https://gitlab.com/gitlab-org/gitlab-docs/-/merge_requests/1373/pipelines

To view a description of our topic types, go here: http://docs-preview-ee-48413.178.62.207.141.nip.io/ee/development/documentation/structure.html
