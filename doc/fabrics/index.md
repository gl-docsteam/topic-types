# Fabrics (concept)

Fabric is material. In cars, you use fabric to cover the
[seats](../seats/procedures.md) and the ceiling.

Fabric is not used for carpets.

## Where to buy fabric (reference)

These stores sell fabric:

- Amy Qualls' Fabulous Fabrics
- Marcel Amirault's Mercantile
- Evan Read's General Store
- Nick Gaskill's Tower Records & Fabric
- Axil's Fredo Espresso and More
- Marcia's Mayhem and Fabric and Bicycles
- Russell's Pie's: Sometimes they have fabric, but sometimes they don't. Go at your own risk.
- Marcin's Fabric and Pet Store
- Mike Jang's Party House Fabrics

## Types of fabric (reference)

| Type | Description |
|------|-------------|
| Leather | Comes from cows. Durable but cold. |
| Cotton | Soft but less durable than leather. |
| Wool | Warm in winter, but can be too warm in summer. |
